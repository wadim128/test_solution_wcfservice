﻿using System;
using System.IO;
using System.Linq;

public class LoginService : ILoginService
{
    private readonly string _dir = AppDomain.CurrentDomain.BaseDirectory;
    public string Login(string login, string password)
    {
        var userData = FindUser(login, password);

        if (userData == null)
            return "Неверный логин или пароль!";
        else
            return "Добрый день, " + login + "!";
    }

    private LoginData FindUser(string login, string password)
    {
        return File.ReadAllLines(_dir + "UserTable.txt")
            .ToList()
            .Select(x =>
            {
                var user = x.Split('\t', ' ');

                if (user.Length < 2)
                    return new LoginData() { Login = user.FirstOrDefault() };
                else
                    return new LoginData() { Login = user.FirstOrDefault(), Password = user.LastOrDefault() };
            })
            .ToList()
            .FirstOrDefault(x => x.Login == login && Cryptography.ComputeSHA256(x.Password) == password);
    }
}
