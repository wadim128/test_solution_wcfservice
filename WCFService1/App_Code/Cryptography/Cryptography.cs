﻿using System.Security.Cryptography;
using System.Text;

public static class Cryptography
{
    public static string ComputeSHA256(string data)
    {
        using (var sha256 = SHA256.Create())
        {
            var bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(data));

            return FormatterByteArray(bytes);
        }
    }

    private static string FormatterByteArray(byte[] data)
    {
        var stringBuilder = new StringBuilder();

        for (int i = 0; i < data.Length; i++)
        {
            stringBuilder.Append(data[i].ToString("x2"));
        }

        return stringBuilder.ToString();
    }
}