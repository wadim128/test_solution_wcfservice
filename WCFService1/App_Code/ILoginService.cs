﻿using System.ServiceModel;
using System.ServiceModel.Web;

[ServiceContract]
public interface ILoginService
{
    [OperationContract]
    [WebInvoke(RequestFormat = WebMessageFormat.Json, 
        ResponseFormat = WebMessageFormat.Json, 
        UriTemplate = "/login", 
        BodyStyle = WebMessageBodyStyle.Wrapped)]
    string Login(string login, string password);
}
